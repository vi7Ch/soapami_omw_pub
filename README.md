# soapAMi: This SOAP client needs a JSON properties file that points to an XML file which contains the soap client request.
It can optionally encrypt a promted password.

[![built with Go1.13.3](https://img.shields.io/badge/built%20with-Go-green.svg)](https://golang.org/)

> **DISCLAIMER**
    THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
    HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


### Version: 0.1 - initial release 01/01/2020 - Viseth Chaing

Current supported environment: Windows / Linux

> **Deployment context** 
- Download the soapAMi.exe or soapAMi binary file and the properties.json files on your server.
- Update the properties.json file according to your needs. Pay attention to the "url", "soapAction", "username", "password" and point to the XML soap request file.
- Create the XML soap payload file.


### Manual Usage:
Copy the binary, the properties.json file and the corresponding XML payload file on your server.

to display usage:
```bash
./soapAMi -h
Usage of soapAMi:
  -about
        bool: author: Viseth CHAING (viseth.chaing@gmail.com)| build: 0.1 | date: 01-01-2020 | print the disclamer
  -config string
        string: a json file name (default "properties.json")
  -encrypt
        bool: true or false to encrypt the password given
  -log string
        string: a output log file name (default "./log/soapAMi.log")

```

To execute a soap request, perform the following:
```bash
./soapAMi -config <properties.json> <eventId1> <eventId2> <eventId3>
```

To encrypt the password (username pwd), use the encrypt flag to encrypt the password, then copy/paste the encrypted string in the "password" field in properties.json files

```bash
./soapAMi -encrypt
Enter a password :
testpwd
Encrypted: d4ad2e2e8fbd7376707d080b6f6188a4ab3bff28c5808efb193fbff63ee79394c1b4c5
Decrypted: testpwd
``` 

### Note:
**Required INPUT files**
- properties.json file
- XML payload


**OUTPUT files**
- log files: this tool logs its activity in a logfile with the below default caracteristics: 
```bash
        - LogLevel: info, warning, error, debug
		- LogMaxAge: 28 days
        - LogMaxSize: 1MB
        - LogMaxBackups: 3 files
		- LogCompress: true
        - Directory of the logfile: 'log' 
        - Name: 'soapAMi.log'

```

